var GSTableController;

GSTableController = (function() {
  function GSTableController(databaseManager) {
    this.databaseManager = databaseManager;
    this.crypto = require('crypto');
    this.uuid = require('node-uuid');
    this._ = require('underscore');
  }

  GSTableController.prototype.hashString = function(str) {
    return this.crypto.createHash('md5').update(str).digest("hex");
  };

  GSTableController.prototype.generateUUID = function() {
    return this.uuid.v4();
  };

  GSTableController.prototype.validateKeysInObject = function(keys, obj) {
    var key, valid, _fn, _i, _len;
    console.log(keys);
    valid = true;
    _fn = function(key) {
      if ((obj[key] != null) === false) {
        return valid = false;
      }
    };
    for (_i = 0, _len = keys.length; _i < _len; _i++) {
      key = keys[_i];
      _fn(key);
    }
    return valid;
  };

  GSTableController.prototype.validateAuthentication = function(data, completionFn) {
    var apiKey, validParams;
    validParams = this.validateKeysInObject(['apiKey'], data);
    if (validParams) {
      apiKey = data.apiKey;
      return this.databaseManager.executeQuery('SELECT user_id FROM user WHERE api_key=:apiKey', {
        apiKey: apiKey
      }, function(result, error, info) {
        var isValid, success;
        success = error === '' ? true : false;
        isValid = false;
        if (result.length > 0 && (result[0].user_id != null) === true) {
          isValid = true;
        }
        if (completionFn) {
          return completionFn(isValid);
        }
      });
    } else {
      if (completionFn) {
        return completionFn(false);
      }
    }
  };

  return GSTableController;

})();
;var GSUserTableController,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

GSUserTableController = (function(_super) {
  __extends(GSUserTableController, _super);

  function GSUserTableController(databaseManager) {
    GSUserTableController.__super__.constructor.call(this, databaseManager);
  }

  GSUserTableController.prototype.createUser = function(data, completionFn) {
    var creationDate, hashedPassword, password, username, uuid, validParams;
    validParams = this.validateKeysInObject(['username', 'password'], data);
    if (validParams) {
      username = data.username;
      password = data.password;
      creationDate = (new Date()).toYMD();
      hashedPassword = this.hashString(username + password);
      uuid = this.generateUUID();
      return this.databaseManager.executeQuery('INSERT INTO user (username, password, uuid, date_joined) VALUES (:username, :password, :uuid, :datejoined); SELECT LAST_INSERT_ID();', {
        username: username,
        password: hashedPassword,
        uuid: uuid,
        datejoined: creationDate
      }, function(result, error, info) {
        var success;
        success = error === '' ? true : false;
        if (completionFn) {
          return completionFn(success, result, error, info);
        }
      });
    } else {
      if (completionFn) {
        return completionFn(false, '', 'Invalid parameters', '');
      }
    }
  };

  GSUserTableController.prototype.getUserById = function(data, completionFn) {
    var userId, validParams;
    validParams = this.validateKeysInObject(['userId'], data);
    if (validParams) {
      userId = data.userId;
      return this.databaseManager.executeQuery('SELECT username, uuid, date_joined FROM user WHERE user_id=:userId', {
        userId: userId
      }, function(result, error, info) {
        var success;
        success = error === '' ? true : false;
        if (completionFn) {
          return completionFn(success, result, error, info);
        }
      });
    } else {
      if (completionFn) {
        return completionFn(false, '', 'Invalid parameters', '');
      }
    }
  };

  GSUserTableController.prototype.generateAPIKey = function() {
    var apiKey;
    return apiKey = this.uuid.v4();
  };

  GSUserTableController.prototype.updateUserAPIKey = function(username, apiKey, completionFn) {
    return this.databaseManager.executeQuery('UPDATE user SET api_key=:apiKey WHERE username=:username', {
      apiKey: apiKey,
      username: username
    }, function(result, error, info) {
      var success;
      success = error === '' ? true : false;
      result = [
        {
          apiKey: apiKey
        }
      ];
      if (completionFn) {
        return completionFn(success, result, error, info);
      }
    });
  };

  GSUserTableController.prototype.authenticateUser = function(data, completionFn) {
    var password, username, validParams;
    validParams = this.validateKeysInObject(['username', 'password'], data);
    if (validParams) {
      username = data.username;
      password = this.hashString(data.username + data.password);
      return this.databaseManager.executeQuery('SELECT username, password FROM user WHERE username=:username', {
        username: username
      }, (function(_this) {
        return function(result, error, info) {
          var apiKey, success, user;
          success = error === '' ? true : false;
          if (success === true && result.length > 0) {
            user = result[0];
            if (user.password === password) {
              apiKey = _this.generateAPIKey();
              return _this.updateUserAPIKey(username, apiKey, completionFn);
            }
          } else {
            if (completionFn) {
              return completionFn(success, result, error, info);
            }
          }
        };
      })(this));
    } else {
      if (completionFn) {
        return completionFn(false, '', 'Invalid parameters', '');
      }
    }
  };

  return GSUserTableController;

})(GSTableController);
;var GSProjectTableController,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

GSProjectTableController = (function(_super) {
  __extends(GSProjectTableController, _super);

  function GSProjectTableController(databaseManager) {
    GSProjectTableController.__super__.constructor.call(this, databaseManager);
  }

  GSProjectTableController.prototype.createProject = function(data, completionFn) {
    return this.validateAuthentication(data, (function(_this) {
      return function(isValid) {
        var creationDate, info, ownerId, title, uuid, validParams;
        if (isValid === true) {
          validParams = _this.validateKeysInObject(['title', 'info', 'userId'], data);
          if (validParams) {
            title = data.title;
            info = data.info;
            ownerId = data.userId;
            creationDate = (new Date()).toYMD();
            uuid = _this.generateUUID();
            return _this.databaseManager.executeQuery('INSERT INTO project (title, info, uuid, date_created, owner_id) VALUES (:title, :info, :uuid, :creationDate, :ownerId); SELECT LAST_INSERT_ID();', {
              title: title,
              info: info,
              uuid: uuid,
              creationDate: creationDate,
              ownerId: ownerId
            }, function(result, error, info) {
              var success;
              success = error === '' ? true : false;
              if (completionFn) {
                return completionFn(success, result, error, info);
              }
            });
          } else {
            if (completionFn) {
              return completionFn(false, '', 'Invalid parameters', '');
            }
          }
        } else {
          if (completionFn) {
            return completionFn(false, '', 'Invalid API key', '');
          }
        }
      };
    })(this));
  };

  GSProjectTableController.prototype.getProjectById = function(data, completionFn) {
    return this.validateAuthentication(data, (function(_this) {
      return function(isValid) {
        var projectId, validParams;
        if (isValid === true) {
          validParams = _this.validateKeysInObject(['projectId'], data);
          if (validParams) {
            projectId = data.projectId;
            return _this.databaseManager.executeQuery('SELECT title, info, uuid, date_created, owner_id FROM project WHERE project_id=:projectId', {
              projectId: projectId
            }, function(result, error, info) {
              var success;
              success = error === '' ? true : false;
              if (completionFn) {
                return completionFn(success, result, error, info);
              }
            });
          } else {
            if (completionFn) {
              return completionFn(false, '', 'Invalid parameters', '');
            }
          }
        } else {
          if (completionFn) {
            return completionFn(false, '', 'Invalid API key', '');
          }
        }
      };
    })(this));
  };

  return GSProjectTableController;

})(GSTableController);
;var GSMessageTableController,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

GSMessageTableController = (function(_super) {
  __extends(GSMessageTableController, _super);

  function GSMessageTableController(databaseManager) {
    GSMessageTableController.__super__.constructor.call(this, databaseManager);
  }

  GSMessageTableController.prototype.postMesageToProject = function(data, completionFn) {
    return this.validateAuthentication(data, (function(_this) {
      return function(isValid) {
        var attachment, content, creationDate, projectId, topic, userId, uuid, validParams;
        if (isValid === true) {
          validParams = _this.validateKeysInObject(['topic', 'content', 'attachment', 'userId', 'projectId'], data);
          if (validParams) {
            topic = data.topic;
            content = data.content;
            attachment = data.attachment;
            userId = data.userId;
            projectId = data.projectId;
            creationDate = (new Date()).toYMD();
            uuid = _this.generateUUID();
            return _this.databaseManager.executeQuery('INSERT INTO message (topic, content, attachment, created_by, project_id, date_created, uuid) VALUES (:topic, :content, :attachment, :userId, :projectId, :creationDate, :uuid); SELECT LAST_INSERT_ID();', {
              topic: topic,
              content: content,
              attachment: attachment,
              userId: userId,
              projectId: projectId,
              creationDate: creationDate,
              uuid: uuid
            }, function(result, error, info) {
              var success;
              success = error === '' ? true : false;
              if (completionFn) {
                return completionFn(success, result, error, info);
              }
            });
          } else {
            if (completionFn) {
              return completionFn(false, '', 'Invalid parameters', '');
            }
          }
        } else {
          if (completionFn) {
            return completionFn(false, '', 'Invalid API key', '');
          }
        }
      };
    })(this));
  };

  GSMessageTableController.prototype.getMessageById = function(data, completionFn) {
    return this.validateAuthentication(data, (function(_this) {
      return function(isValid) {
        var messageId, validParams;
        if (isValid === true) {
          validParams = _this.validateKeysInObject(['messageId'], data);
          if (validParams) {
            messageId = data.messageId;
            return _this.databaseManager.executeQuery('SELECT topic, content, attachment, created_by, project_id, date_created, uuid FROM message WHERE message_id=:messageId', {
              messageId: messageId
            }, function(result, error, info) {
              var success;
              success = error === '' ? true : false;
              if (completionFn) {
                return completionFn(success, result, error, info);
              }
            });
          } else {
            if (completionFn) {
              return completionFn(false, '', 'Invalid parameters', '');
            }
          }
        } else {
          if (completionFn) {
            return completionFn(false, '', 'Invalid API key', '');
          }
        }
      };
    })(this));
  };

  GSMessageTableController.prototype.getMessagesForProject = function(data, completionFn) {
    return this.validateAuthentication(data, (function(_this) {
      return function(isValid) {
        var projectId, validParams;
        if (isValid === true) {
          validParams = _this.validateKeysInObject(['projectId'], data);
          if (validParams) {
            projectId = data.projectId;
            return _this.databaseManager.executeQuery('SELECT topic, content, attachment, created_by, project_id, date_created, uuid FROM message WHERE project_id=:projectId', {
              projectId: projectId
            }, function(result, error, info) {
              var success;
              success = error === '' ? true : false;
              if (completionFn) {
                return completionFn(success, result, error, info);
              }
            });
          } else {
            if (completionFn) {
              return completionFn(false, '', 'Invalid parameters', '');
            }
          }
        } else {
          if (completionFn) {
            return completionFn(false, '', 'Invalid API key', '');
          }
        }
      };
    })(this));
  };

  return GSMessageTableController;

})(GSTableController);
;var GSRouter;

GSRouter = (function() {
  function GSRouter(app) {
    this.util = require('util');
    this.app = app;
    this.tableController = null;
  }

  GSRouter.prototype.echo = function(req, res) {
    return res.send('' + this.util.inspect(req.body.message));
  };

  return GSRouter;

})();
;var GSUserRouter,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

GSUserRouter = (function(_super) {
  __extends(GSUserRouter, _super);

  function GSUserRouter(app, databaseManager) {
    GSUserRouter.__super__.constructor.call(this, app);
    this.tableController = new GSUserTableController(databaseManager);
    this.app.post('/createUser', (function(_this) {
      return function(req, res) {
        return _this.createUser(req, res);
      };
    })(this));
    this.app.post('/getUserById', (function(_this) {
      return function(req, res) {
        return _this.getUserById(req, res);
      };
    })(this));
    this.app.post('/authenticateUser', (function(_this) {
      return function(req, res) {
        return _this.authenticateUser(req, res);
      };
    })(this));
  }

  GSUserRouter.prototype.createUser = function(req, res) {
    return this.tableController.createUser(req.body, (function(_this) {
      return function(success, result, error, info) {
        return res.send('' + _this.util.inspect({
          success: success,
          result: result,
          error: error,
          info: info
        }));
      };
    })(this));
  };

  GSUserRouter.prototype.getUserById = function(req, res) {
    return this.tableController.getUserById(req.body, (function(_this) {
      return function(success, result, error, info) {
        return res.send('' + _this.util.inspect({
          success: success,
          result: result,
          error: error,
          info: info
        }));
      };
    })(this));
  };

  GSUserRouter.prototype.authenticateUser = function(req, res) {
    return this.tableController.authenticateUser(req.body, (function(_this) {
      return function(success, result, error, info) {
        return res.send('' + _this.util.inspect({
          success: success,
          result: result,
          error: error,
          info: info
        }));
      };
    })(this));
  };

  return GSUserRouter;

})(GSRouter);
;var GSProjectRouter,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

GSProjectRouter = (function(_super) {
  __extends(GSProjectRouter, _super);

  function GSProjectRouter(app, databaseManager) {
    GSProjectRouter.__super__.constructor.call(this, app);
    this.tableController = new GSProjectTableController(databaseManager);
    this.app.post('/createProject', (function(_this) {
      return function(req, res) {
        return _this.createProject(req, res);
      };
    })(this));
    this.app.post('/getProjectById', (function(_this) {
      return function(req, res) {
        return _this.getProjectById(req, res);
      };
    })(this));
  }

  GSProjectRouter.prototype.createProject = function(req, res) {
    return this.tableController.createProject(req.body, (function(_this) {
      return function(success, result, error, info) {
        return res.send('' + _this.util.inspect({
          success: success,
          result: result,
          error: error,
          info: info
        }));
      };
    })(this));
  };

  GSProjectRouter.prototype.getProjectById = function(req, res) {
    return this.tableController.getProjectById(req.body, (function(_this) {
      return function(success, result, error, info) {
        return res.send('' + _this.util.inspect({
          success: success,
          result: result,
          error: error,
          info: info
        }));
      };
    })(this));
  };

  return GSProjectRouter;

})(GSRouter);
;var GSMessageRouter,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

GSMessageRouter = (function(_super) {
  __extends(GSMessageRouter, _super);

  function GSMessageRouter(app, databaseManager) {
    GSMessageRouter.__super__.constructor.call(this, app);
    this.tableController = new GSMessageTableController(databaseManager);
    this.app.post('/postMesageToProject', (function(_this) {
      return function(req, res) {
        return _this.postMesageToProject(req, res);
      };
    })(this));
    this.app.post('/getMessagesForProject', (function(_this) {
      return function(req, res) {
        return _this.getMessagesForProject(req, res);
      };
    })(this));
  }

  GSMessageRouter.prototype.postMesageToProject = function(req, res) {
    return this.tableController.postMesageToProject(req.body, (function(_this) {
      return function(success, result, error, info) {
        return res.send('' + _this.util.inspect({
          success: success,
          result: result,
          error: error,
          info: info
        }));
      };
    })(this));
  };

  GSMessageRouter.prototype.getMessagesForProject = function(req, res) {
    return this.tableController.getMessagesForProject(req.body, (function(_this) {
      return function(success, result, error, info) {
        return res.send('' + _this.util.inspect({
          success: success,
          result: result,
          error: error,
          info: info
        }));
      };
    })(this));
  };

  return GSMessageRouter;

})(GSRouter);
;var GSDatabaseManager;

GSDatabaseManager = (function() {
  function GSDatabaseManager() {
    var Client;
    this.util = require('util');
    Client = require('mariasql');
    this.databaseClient = new Client();
    this.databaseClient.on('connect', this.onConnect).on('error', this.onError).on('close', this.onClose);
  }

  GSDatabaseManager.prototype.establishConnection = function() {
    return this.databaseClient.connect({
      host: 'localhost',
      user: 'root',
      password: 'root',
      db: 'geyserdb',
      multiStatements: true
    });
  };

  GSDatabaseManager.prototype.onConnect = function() {
    return console.log('Client connected');
  };

  GSDatabaseManager.prototype.onError = function(err) {
    return console.log('Client error: ' + err);
  };

  GSDatabaseManager.prototype.onClose = function(hadError) {
    return console.log('Client closed');
  };

  GSDatabaseManager.prototype.executeQuery = function(query, substitions, completionFn) {
    var error, info, result, rows;
    result = '';
    error = '';
    info = '';
    rows = [];
    return this.databaseClient.query(query, substitions).on('result', (function(_this) {
      return function(res) {
        return res.on('row', function(row) {
          console.log('Result row: ' + _this.util.inspect(row));
          return rows.push(row);
        }).on('error', function(err) {
          console.log('Result error: ' + _this.util.inspect(err));
          return error = err;
        }).on('end', function(info) {
          console.log('Result finished successfully' + _this.util.inspect(info));
          return info = info;
        });
      };
    })(this)).on('end', (function(_this) {
      return function() {
        console.log('Done with all results');
        result = rows;
        return completionFn(result, error, info);
      };
    })(this));
  };

  return GSDatabaseManager;

})();
;var Date_toYMD, app, bodyParser, databaseManager, http, io, messageRouter, port, projectRouter, userRouter, util;

Date_toYMD = function() {
  var day, hours, minutes, month, seconds, year;
  year = '' + this.getFullYear();
  month = '' + (this.getMonth() + 1);
  if (month.length === 1) {
    month = "0" + month;
  }
  day = '' + this.getDate();
  if (day.length === 1) {
    day = "0" + day;
  }
  hours = this.getHours();
  if (hours.length === 1) {
    hours = "0" + hours;
  }
  minutes = this.getMinutes();
  if (minutes.length === 1) {
    minutes = "0" + minutes;
  }
  seconds = this.getSeconds();
  if (seconds.length === 1) {
    seconds = "0" + seconds;
  }
  return year + "-" + month + "-" + day + ' ' + hours + ':' + minutes + ':' + seconds;
};

Date.prototype.toYMD = Date_toYMD;

port = process.env.PORT || 8081;

app = require('express')();

http = require('http').Server(app);

io = require('socket.io')(http);

util = require('util');

bodyParser = require('body-parser');

databaseManager = new GSDatabaseManager();

databaseManager.establishConnection();

app.use(bodyParser.urlencoded());

app.use(bodyParser.json());

app.use(bodyParser.json({
  type: 'application/vnd.api+json'
}));

app.get('/', function(req, res) {
  return res.send('Hello World');
});

app.post('/', function(req, res) {
  return res.send('World Hello');
});

io.on('connection', function(socket) {
  return console.log('a user connected');
});

http.listen(port, function() {
  return console.log('listening on *:' + port);
});

userRouter = new GSUserRouter(app, databaseManager);

projectRouter = new GSProjectRouter(app, databaseManager);

messageRouter = new GSMessageRouter(app, databaseManager);
;
//# sourceMappingURL=GSApp.js.map