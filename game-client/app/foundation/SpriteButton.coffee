class SpriteButton extends Phaser.Group
  constructor: (game, x, y, normalImage, pressedImage, title, clickFn) ->
    Phaser.Group.call(this, game);

    @game = game
    @normalImage = normalImage
    @pressedImage = pressedImage

    emptyFn = () ->
      #Empty
    @clickFn = if clickFn? is true then clickFn else emptyFn

    @buttonBackgroundSprite = new Phaser.Sprite(@game, x, y, @normalImage);
    @buttonBackgroundSprite.anchor.setTo(0.5, 0.5);

    style = { font: "24px Arial", fill: "#ffffff", align: "center" };
    titleText = if title? then title else ''
    @buttonTitleText = new Phaser.Text(@game, x, y, titleText, style);
    @buttonTitleText.anchor.set(0.5);

    @add(@buttonBackgroundSprite)
    @add(@buttonTitleText)

    @buttonBackgroundSprite.inputEnabled = true
    @buttonBackgroundSprite.events.onInputDown.add(() ->
      if @pressedImage
        @buttonBackgroundSprite.loadTexture(@pressedImage);
    , this);
    @buttonBackgroundSprite.events.onInputUp.add(() ->
      @buttonBackgroundSprite.loadTexture(@normalImage);
      if @clickFn
        @clickFn()
    , this);
