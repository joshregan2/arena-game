class GameController
  constructor: (io) ->
    @_ = require('underscore')
    @util = require('util')
    @uuid = require('node-uuid')
    @io = io
    @games = []

  playerConnected: (socket) =>
    socket.on('requestGamesList', (data) =>
      @requestGamesList(socket, data)
    );
    socket.on('hostCreateGame', (data) =>
      @hostCreateGame(socket, data)
    );
    socket.on('sendMessageToGame', (data) =>
      @sendMessageToGame(socket, data)
    );
    socket.on('joinGame', (data) =>
      @joinGame(socket, data)
    );
    socket.on('leaveGame', (data) =>
      @leaveGame(socket, data)
    );

    console.log('A player connected')


  requestGamesList: (socket, data) =>
    @io.emit('gamesListUpdate', JSON.stringify(@games));

  hostCreateGame: (socket, data) =>
    gameId = @uuid.v4()
    @games.push(gameId)
    socket.join(gameId)
    socket.gameId = gameId
    console.log(gameId)
    @io.emit('gamesListUpdate', JSON.stringify(@games));

  sendMessageToGame: (socket, data) =>
    @io.to(socket.gameId).emit('gameData', data)

  joinGame: (socket, data) =>
    gameId = data.gameId
    if gameId?
      playerId = @findClientsSocket(''+gameId).length
      socket.gameId = gameId
      socket.emit('joinedGame', playerId)
      @io.to(gameId).emit('playerJoinedGame', playerId)
      socket.join(gameId);
      console.log('Player joined game with id '+playerId)

  leaveGame: (socket, data) =>
    gameId = socket.gameId
    if gameId?
      socket.leave(gameId);
      @io.to(gameId).emit('playerLeftGame', '')

  findClientsSocket: (roomId) =>
    clients = []
    ns = @io.of("/");

    if ns?
      for id of ns.connected
        do (id) =>
          if roomId?
            index = ns.connected[id].rooms.indexOf(roomId)
            if index isnt -1
              clients.push(ns.connected[id])
            else
              clients.push(ns.connected[id]);
    return clients;
