class GameMap extends Phaser.Tilemap
  constructor: (game, mapName) ->
    Phaser.Tilemap.call(this, game, mapName, 32, 32, 1024, 768)

    # Subclass this class to make maps, look at TestMap.coffee for an example
