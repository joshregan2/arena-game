class TestMap extends GameMap
  constructor: (game) ->
    # Pass in the name of the json tilemap to the super constructor
    super(game, 'test-map')

    # Map Specific Setup
    @addTilesetImage('basic', 'basic-tilemap')

    @layer1 = @createLayer('Tile Layer 1')
    @layer2 = @createLayer('Tile Layer 2')

    # @layer1.debug = true

    @layer1.resizeWorld()
    @layer2.resizeWorld()
