class ExampleScene extends Scene
  constructor: (game) ->
    # Initialise scene
    super(game)

    @logoSprite = null

  start: () ->
    # Perform start up logic here
    super()

    # Let's make a test sprite to display
    @logoSprite = @game.add.sprite(@game.world.centerX, @game.world.centerY, 'logo');
    @logoSprite.anchor.setTo(0.5, 0.5);

  destroy: () ->
    # Call this to destroy this and any references
    super()

    # Eg. Destroy any sprites and their children
    @logoSprite.destroy(true)

  update: () ->
    # Update loop
    super()

  render: () ->
    # Render loop
    super()
