class GameManager
  constructor: (screenWidth, screenHeight, rootSceneType) ->
    # Game setup
    @networkManager = new NetworkManager()
    @game = new Phaser.Game(screenWidth, screenHeight, Phaser.AUTO, '', { preload: @preload, create: @start, update: @update, render: @render })
    @currentScene = if rootSceneType? then new rootSceneType(@game) else null

    @networkManager.connectToGameServer()

  preload: () =>
    # Preload assets here
    @game.load.image('logo', '/img/phaser.png')
    @game.load.image('test', '/img/test-head.png')
    # @game.load.image('taco-monstro', '/img/taco-monstro.png')
    @game.load.spritesheet('taco-monstro', '/img/taco-monstro-walk.png', 36, 62, 3);
    @game.load.image('button-blue', '/img/menu-button-blue.png')
    @game.load.image('button-green', '/img/menu-button-green.png')
    @game.load.image('button-pink', '/img/menu-button-pink.png')
    @game.load.image('button-blue-pressed', '/img/menu-button-blue-pressed.png')
    @game.load.image('button-green-pressed', '/img/menu-button-green-pressed.png')
    @game.load.image('button-pink-pressed', '/img/menu-button-pink-pressed.png')
    @game.load.image('game-selector-button-on', '/img/game-selector-button-on.png')
    @game.load.image('game-selector-button-off', '/img/game-selector-button-off.png')
    @game.load.image('basic-tilemap', '/img/basic-tilemap.png');
    @game.load.tilemap('test-map', '/img/test-map.json', null, Phaser.Tilemap.TILED_JSON);
    @game.load.image('training-arena', '/img/training-arena.png');
    @game.load.tilemap('battleArea1', '/img/battleArea1.json', null, Phaser.Tilemap.TILED_JSON);


  start: () =>
    # Initialisation here
    if @currentScene?
      @currentScene.start()

  update: () =>
    # Update loop
    if @currentScene?
      @currentScene.update()

  render: () =>
    # Render loop
    if @currentScene?
      @currentScene.render()

  showSceneType: (sceneType, params) ->
    if sceneType?
      if @currentScene?
        @currentScene.destroy()
      @currentScene = new sceneType(@game, params)
      @currentScene.start()
