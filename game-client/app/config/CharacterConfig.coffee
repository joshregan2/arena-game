class CharacterConfig
  constructor: () ->
    @CHARACTER_TYPES = {
      TACO_MONSTRO: 'TACO_MONSTRO'
    }

  getImageForCharacter: (characterType) ->
    switch characterType
      when @CHARACTER_TYPES.TACO_MONSTRO then return 'taco-monstro'

    return 'test'

window.SharedCharacterConfig = new CharacterConfig()
