class TitleScene
  constructor: (game) ->
    # Initialise scene
    @game = game



  start: () ->
    # Perform start up logic here
    @hostGameButton = new SpriteButton(
      @game,
      @game.world.centerX,
      200,
      'button-blue',
      'button-blue-pressed',
      'Host Game',
      @pressedHostGameButton
    )
    @joinGameButton = new SpriteButton(
      @game,
      @game.world.centerX,
      300,
      'button-pink',
      'button-pink-pressed',
      'Join Game',
      @pressedJoinGameButton
    )

  destroy: () ->
    # Call this to destroy this and any references
    @hostGameButton.destroy(true)
    @joinGameButton.destroy(true)

  update: () ->
    # Update loop

  render: () ->
    # Render loop

  pressedHostGameButton: () ->
    # Show lobby scene or go into a game people can join
    if SharedGameManager.networkManager.isConnected is true
      SharedGameManager.networkManager.hostNewGame()
    SharedGameManager.showSceneType(MainGameScene, { isHost: true })

  pressedJoinGameButton: () ->
    # Show game list scene
    SharedGameManager.showSceneType(GameListScene)
