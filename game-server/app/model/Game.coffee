class Game
  constructor: (hostId, gameId) ->
    @gameId = gameId
    @hostId = hostId
    @playerIds = []
    @status = 'LOBBY'

  addPlayer: (playerId) ->
    @playerIds.push(playerId)

  removePlayer: (playerId) ->
    playerIndex = @playerIds.indexOf(playerId)
    if playerIndex isnt -1
      @playerIds.splice(playerIndex,1)
