class PlayerCharacter extends Phaser.Group
  constructor: (game, x, y, characterType, isPlayable) ->
    Phaser.Group.call(this, game);
    @game = game
    @isPlayable = isPlayable
    @characterType = characterType
    @characterStats = new CharacterStats()
    characterImg = SharedCharacterConfig.getImageForCharacter(characterType)

    @characterSprite = new Phaser.Sprite(@game, x, y, characterImg)
    @characterSprite.anchor.setTo(0.5, 0.5)

    @characterSprite.animations.add('walkDown', [0,1,2]);

    @add(@characterSprite)

    @game.physics.enable(@characterSprite, Phaser.Physics.ARCADE)
    @characterSprite.body.collideWorldBounds = true

  configureForCharacterType: (characterType) ->
    # Change stats etc. here

  update: () ->
    super()

    if @isPlayable
      # WASD Test Controls
      if @game.input.keyboard.isDown(Phaser.Keyboard.A)
        @characterSprite.body.velocity.x = -@characterStats.movementSpeed;
        @playWalkDownAnimation()
      else if @game.input.keyboard.isDown(Phaser.Keyboard.D)
        @characterSprite.body.velocity.x = @characterStats.movementSpeed;
        @playWalkDownAnimation()
      else
        @characterSprite.body.velocity.x = 0
        # @characterSprite.animations.stop(null, true)

      if @game.input.keyboard.isDown(Phaser.Keyboard.W)
        @characterSprite.body.velocity.y = -@characterStats.movementSpeed;
        @playWalkDownAnimation()
      else if @game.input.keyboard.isDown(Phaser.Keyboard.S)
        @characterSprite.body.velocity.y = @characterStats.movementSpeed;
        @playWalkDownAnimation()
      else
        @characterSprite.body.velocity.y = 0;

  playWalkDownAnimation: () ->
      @characterSprite.animations.play('walkDown', 6, false)
