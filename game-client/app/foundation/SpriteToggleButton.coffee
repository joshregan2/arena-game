class SpriteToggleButton extends Phaser.Group
  constructor: (game, x, y, offImage, onImage, title, clickFn) ->
    Phaser.Group.call(this, game);

    @game = game
    @isOn = false
    @offImage = offImage
    @onImage = onImage
    emptyFn = () ->
      #Empty
    @clickFn = if clickFn? is true then clickFn else emptyFn

    @buttonBackgroundSprite = new Phaser.Sprite(@game, x, y, @offImage);
    @buttonBackgroundSprite.anchor.setTo(0.5, 0.5);

    style = { font: "24px Arial", fill: "#ffffff", align: "center" };
    titleText = if title? then title else ''
    @buttonTitleText = new Phaser.Text(@game, x, y, titleText, style);
    @buttonTitleText.anchor.set(0.5);

    @add(@buttonBackgroundSprite)
    @add(@buttonTitleText)

    @buttonBackgroundSprite.inputEnabled = true
    @buttonBackgroundSprite.events.onInputUp.add(() ->
      @toggle()
      if @clickFn?
        @clickFn()
    ,this);

  toggle: () ->
    if @isOn is true
      @toggleOff()
    else
      @toggleOn()

  toggleOn: () ->
    @isOn = true
    @buttonBackgroundSprite.loadTexture(@onImage);

  toggleOff: () ->
    @isOn = false
    @buttonBackgroundSprite.loadTexture(@offImage);
