exports.config =
  # See http://brunch.io/#documentation for docs.

  paths:
    public: './build'
    watched: [
      'app',
      'assets',
      'style',
      'vendor',
      'bower_components'
    ]

  modules:
    definition: false
    wrapper: false

  files:
    javascripts:
      joinTo:
        'app/Server.js': /^app/
        'app/vendor.js': /^vendor/
      order:
        before: [
          'app/controllers/GameController.coffee'
        ]
        after: [
          'app/Server.coffee'
        ]
    stylesheets:
      joinTo: 'app.css'
    templates:
      joinTo: 'app.js'

  server:
    path: 'Server.coffee'
    port: 3333
    base: '/'
    run: yes
