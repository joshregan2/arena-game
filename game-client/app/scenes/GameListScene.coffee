class GameListScene
  constructor: (game) ->
    # Initialise scene
    @game = game

    @availableGames = ['test','test','test','test','test']
    @availableGameToggleButtons = []

    @currentSelectedGame = null

  start: () ->
    # Perform start up logic here
    @joinGameButton = new SpriteButton(
      @game,
      @game.world.centerX - 256,
      @game.world.height - 160,
      'button-blue',
      'button-blue-pressed',
      'Join Game',
      @pressedJoinGameButton
    )
    @backButton = new SpriteButton(
      @game,
      @game.world.centerX + 256,
      @game.world.height - 160,
      'button-pink',
      'button-pink-pressed',
      'Back to Menu',
      @pressedBackButton
    )

    # Listen for games
    if SharedGameManager.networkManager.isConnected is true
      SharedGameManager.networkManager.socket.on('gamesListUpdate', @updateGameList)
      SharedGameManager.networkManager.requestGamesList()
    # @updateGameList()

  destroy: () ->
    # Call this to destroy this and any references
    if SharedGameManager.networkManager.socket?
      SharedGameManager.networkManager.socket.removeListener('gamesListUpdate', @updateGameList)

    @joinGameButton.destroy(true)
    @backButton.destroy(true)
    for gameToggleButton in @availableGameToggleButtons
      do (gameToggleButton) =>
        gameToggleButton.destroy(true)

  update: () ->
    # Update loop

  render: () ->
    # Render loop

  pressedJoinGameButton: () =>
    # Join game stored in @currentSelectedGame
    if @currentSelectedGame? is true and SharedGameManager.networkManager.isConnected is true
      SharedGameManager.networkManager.socket.on('joinedGame', @joinedGame)
      SharedGameManager.networkManager.joinGame(@currentSelectedGame)


  pressedBackButton: () ->
    # Show title scene
    SharedGameManager.showSceneType(TitleScene)

  joinedGame: (data) =>
    SharedGameManager.networkManager.socket.removeListener('joinedGame', @joinedGame)
    SharedGameManager.showSceneType(MainGameScene, { isHost: false, playerId: data })

  selectGame: (selectedButton, selectedGame) ->
    # Handle game selection
    @currentSelectedGame = selectedGame
    for gameToggleButton in @availableGameToggleButtons
      do (gameToggleButton) =>
        if gameToggleButton isnt selectedButton
          gameToggleButton.toggleOff()

  updateGameList: (data) =>
    @availableGames = JSON.parse(data)

    current = @currentSelectedGame
    @currentSelectedGame = null
    for gameToggleButton in @availableGameToggleButtons
      do (gameToggleButton) =>
        gameToggleButton.destroy(true)

    @availableGameToggleButtons = []

    yCount = 0
    xCount = 0
    for g in @availableGames
      do (g) =>
        toggleButton = new SpriteToggleButton(
          @game,
          224 + (288 * xCount),
          128 + (64 * yCount),
          'game-selector-button-off',
          'game-selector-button-on',
          ''+g,
          null
        )
        toggleButton.clickFn = () =>
          @selectGame(toggleButton, g)
        @availableGameToggleButtons.push(toggleButton)

        if g is current
          @selectGame(toggleButton, g)
          toggleButton.toggleOn()

        xCount++
        if xCount > 2
          xCount = 0
          yCount++
