# Setup Dependencies
port = (process.env.PORT || 8081)
app = require('express')()
http = require('http').Server(app)
io = require('socket.io')(http)
util = require('util')
bodyParser = require('body-parser')

gameController = new GameController(io)

# Configure HTTP Listener
io.on('connection', (socket) ->
  gameController.playerConnected(socket)
)

http.listen(port, () ->
  console.log('listening on *:'+port)
)
