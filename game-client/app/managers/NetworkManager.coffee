class NetworkManager
  constructor: () ->
    @isConnected = false
    @defaultServerAddress = 'http://192.168.0.207:8081'
    @socket = null
    @playerId = null
    @gameId = null

  connectToGameServer: () ->
    @socket = io(@defaultServerAddress)
    @socket.on('connect', @onConnect)

  onConnect: () =>
    @isConnected = true
    console.log('Connected to game server')
    @socket.on('event', @onEvent)
    @socket.on('disconnect', @onDisconnect)

    # @socket.on('gamesListUpdate', @onGamesListUpdate)
    # @socket.on('joinedGame', @joinedGame)
    # @socket.on('playerJoinedGame', @onPlayerJoinedGame)
    # @socket.on('gameData', @onGameData)

  onEvent: (data) =>
    console.log('Recieved event')

  onDisconnect: () =>
    console.log('Disconnected from game server')
    @isConnected = false


  onGamesListUpdate: (gameList) =>
    console.log('Available games:')
    console.log(gameList)

  onJoinedGame: (data) =>
    console.log('Joined game with playerId '+data)

  onPlayerJoinedGame: (data) =>
    console.log('Player '+data+' joined game')

  onGameData: (data) =>
    # console.log(data)


  hostNewGame: () =>
    @socket.emit('hostCreateGame', {})

  sendMessageToGame: (message) =>
    @socket.emit('sendMessageToGame', message)

  requestGamesList: () =>
    @socket.emit('requestGamesList', {})

  joinGame: (gameId) =>
    @gameId = gameId
    @socket.emit('joinGame', { gameId: gameId })

  leaveGame: () =>
    @socket.emit('leaveGame', {})
