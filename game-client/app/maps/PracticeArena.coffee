class PracticeArena extends GameMap
  constructor: (game) ->
    # Pass in the name of the json tilemap to the super constructor
    super(game, 'battleArea1')

    # Map Specific Setup
    @addTilesetImage('training-arena-tileset', 'training-arena')

    @layer1 = @createLayer('Tile Layer 1')
#    @layer2 = @createLayer('Tile Layer 2')

    # @layer1.debug = true

    @layer1.resizeWorld()
#    @layer2.resizeWorld()