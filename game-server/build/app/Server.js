var GameController,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

GameController = (function() {
  function GameController(io) {
    this.findClientsSocket = __bind(this.findClientsSocket, this);
    this.leaveGame = __bind(this.leaveGame, this);
    this.joinGame = __bind(this.joinGame, this);
    this.sendMessageToGame = __bind(this.sendMessageToGame, this);
    this.hostCreateGame = __bind(this.hostCreateGame, this);
    this.requestGamesList = __bind(this.requestGamesList, this);
    this.playerConnected = __bind(this.playerConnected, this);
    this._ = require('underscore');
    this.util = require('util');
    this.uuid = require('node-uuid');
    this.io = io;
    this.games = [];
  }

  GameController.prototype.playerConnected = function(socket) {
    socket.on('requestGamesList', (function(_this) {
      return function(data) {
        return _this.requestGamesList(socket, data);
      };
    })(this));
    socket.on('hostCreateGame', (function(_this) {
      return function(data) {
        return _this.hostCreateGame(socket, data);
      };
    })(this));
    socket.on('sendMessageToGame', (function(_this) {
      return function(data) {
        return _this.sendMessageToGame(socket, data);
      };
    })(this));
    socket.on('joinGame', (function(_this) {
      return function(data) {
        return _this.joinGame(socket, data);
      };
    })(this));
    socket.on('leaveGame', (function(_this) {
      return function(data) {
        return _this.leaveGame(socket, data);
      };
    })(this));
    return console.log('A player connected');
  };

  GameController.prototype.requestGamesList = function(socket, data) {
    return this.io.emit('gamesListUpdate', JSON.stringify(this.games));
  };

  GameController.prototype.hostCreateGame = function(socket, data) {
    var gameId;
    gameId = this.uuid.v4();
    this.games.push(gameId);
    socket.join(gameId);
    socket.gameId = gameId;
    console.log(gameId);
    return this.io.emit('gamesListUpdate', JSON.stringify(this.games));
  };

  GameController.prototype.sendMessageToGame = function(socket, data) {
    return this.io.to(socket.gameId).emit('gameData', data);
  };

  GameController.prototype.joinGame = function(socket, data) {
    var gameId, playerId;
    gameId = data.gameId;
    if (gameId != null) {
      playerId = this.findClientsSocket('' + gameId).length;
      socket.gameId = gameId;
      socket.emit('joinedGame', playerId);
      this.io.to(gameId).emit('playerJoinedGame', playerId);
      socket.join(gameId);
      return console.log('Player joined game with id ' + playerId);
    }
  };

  GameController.prototype.leaveGame = function(socket, data) {
    var gameId;
    gameId = socket.gameId;
    if (gameId != null) {
      socket.leave(gameId);
      return this.io.to(gameId).emit('playerLeftGame', '');
    }
  };

  GameController.prototype.findClientsSocket = function(roomId) {
    var clients, id, ns, _fn;
    clients = [];
    ns = this.io.of("/");
    if (ns != null) {
      _fn = (function(_this) {
        return function(id) {
          var index;
          if (roomId != null) {
            index = ns.connected[id].rooms.indexOf(roomId);
            if (index !== -1) {
              return clients.push(ns.connected[id]);
            } else {
              return clients.push(ns.connected[id]);
            }
          }
        };
      })(this);
      for (id in ns.connected) {
        _fn(id);
      }
    }
    return clients;
  };

  return GameController;

})();
;var Game;

Game = (function() {
  function Game(hostId, gameId) {
    this.gameId = gameId;
    this.hostId = hostId;
    this.playerIds = [];
    this.status = 'LOBBY';
  }

  Game.prototype.addPlayer = function(playerId) {
    return this.playerIds.push(playerId);
  };

  Game.prototype.removePlayer = function(playerId) {
    var playerIndex;
    playerIndex = this.playerIds.indexOf(playerId);
    if (playerIndex !== -1) {
      return this.playerIds.splice(playerIndex, 1);
    }
  };

  return Game;

})();
;var Player;

Player = (function() {
  function Player(playerId, socket) {
    this.playerId = playerId;
    this.socket = socket;
  }

  return Player;

})();
;var app, bodyParser, gameController, http, io, port, util;

port = process.env.PORT || 8081;

app = require('express')();

http = require('http').Server(app);

io = require('socket.io')(http);

util = require('util');

bodyParser = require('body-parser');

gameController = new GameController(io);

io.on('connection', function(socket) {
  return gameController.playerConnected(socket);
});

http.listen(port, function() {
  return console.log('listening on *:' + port);
});
;
//# sourceMappingURL=Server.js.map