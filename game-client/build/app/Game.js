var CharacterStats;

CharacterStats = (function() {
  function CharacterStats() {
    this.currentHP = 0;
    this.maxHP = 0;
    this.currentScore = 0;
    this.movementSpeed = 100;
  }

  return CharacterStats;

})();
;var PlayerCharacter,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

PlayerCharacter = (function(_super) {
  __extends(PlayerCharacter, _super);

  function PlayerCharacter(game, x, y, characterType, isPlayable) {
    var characterImg;
    Phaser.Group.call(this, game);
    this.game = game;
    this.isPlayable = isPlayable;
    this.characterType = characterType;
    this.characterStats = new CharacterStats();
    characterImg = SharedCharacterConfig.getImageForCharacter(characterType);
    this.characterSprite = new Phaser.Sprite(this.game, x, y, characterImg);
    this.characterSprite.anchor.setTo(0.5, 0.5);
    this.characterSprite.animations.add('walkDown', [0, 1, 2]);
    this.add(this.characterSprite);
    this.game.physics.enable(this.characterSprite, Phaser.Physics.ARCADE);
    this.characterSprite.body.collideWorldBounds = true;
  }

  PlayerCharacter.prototype.configureForCharacterType = function(characterType) {};

  PlayerCharacter.prototype.update = function() {
    PlayerCharacter.__super__.update.call(this);
    if (this.isPlayable) {
      if (this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
        this.characterSprite.body.velocity.x = -this.characterStats.movementSpeed;
        this.playWalkDownAnimation();
      } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
        this.characterSprite.body.velocity.x = this.characterStats.movementSpeed;
        this.playWalkDownAnimation();
      } else {
        this.characterSprite.body.velocity.x = 0;
      }
      if (this.game.input.keyboard.isDown(Phaser.Keyboard.W)) {
        this.characterSprite.body.velocity.y = -this.characterStats.movementSpeed;
        return this.playWalkDownAnimation();
      } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.S)) {
        this.characterSprite.body.velocity.y = this.characterStats.movementSpeed;
        return this.playWalkDownAnimation();
      } else {
        return this.characterSprite.body.velocity.y = 0;
      }
    }
  };

  PlayerCharacter.prototype.playWalkDownAnimation = function() {
    return this.characterSprite.animations.play('walkDown', 6, false);
  };

  return PlayerCharacter;

})(Phaser.Group);
;var Scene;

Scene = (function() {
  function Scene(game) {
    this.game = game;
  }

  Scene.prototype.start = function() {};

  Scene.prototype.destroy = function() {};

  Scene.prototype.update = function() {};

  Scene.prototype.render = function() {};

  return Scene;

})();
;var GameManager,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

GameManager = (function() {
  function GameManager(screenWidth, screenHeight, rootSceneType) {
    this.render = __bind(this.render, this);
    this.update = __bind(this.update, this);
    this.start = __bind(this.start, this);
    this.preload = __bind(this.preload, this);
    this.networkManager = new NetworkManager();
    this.game = new Phaser.Game(screenWidth, screenHeight, Phaser.AUTO, '', {
      preload: this.preload,
      create: this.start,
      update: this.update,
      render: this.render
    });
    this.currentScene = rootSceneType != null ? new rootSceneType(this.game) : null;
    this.networkManager.connectToGameServer();
  }

  GameManager.prototype.preload = function() {
    this.game.load.image('logo', '/img/phaser.png');
    this.game.load.image('test', '/img/test-head.png');
    this.game.load.spritesheet('taco-monstro', '/img/taco-monstro-walk.png', 36, 62, 3);
    this.game.load.image('button-blue', '/img/menu-button-blue.png');
    this.game.load.image('button-green', '/img/menu-button-green.png');
    this.game.load.image('button-pink', '/img/menu-button-pink.png');
    this.game.load.image('button-blue-pressed', '/img/menu-button-blue-pressed.png');
    this.game.load.image('button-green-pressed', '/img/menu-button-green-pressed.png');
    this.game.load.image('button-pink-pressed', '/img/menu-button-pink-pressed.png');
    this.game.load.image('game-selector-button-on', '/img/game-selector-button-on.png');
    this.game.load.image('game-selector-button-off', '/img/game-selector-button-off.png');
    this.game.load.image('basic-tilemap', '/img/basic-tilemap.png');
    this.game.load.tilemap('test-map', '/img/test-map.json', null, Phaser.Tilemap.TILED_JSON);
    this.game.load.image('training-arena', '/img/training-arena.png');
    return this.game.load.tilemap('battleArea1', '/img/battleArea1.json', null, Phaser.Tilemap.TILED_JSON);
  };

  GameManager.prototype.start = function() {
    if (this.currentScene != null) {
      return this.currentScene.start();
    }
  };

  GameManager.prototype.update = function() {
    if (this.currentScene != null) {
      return this.currentScene.update();
    }
  };

  GameManager.prototype.render = function() {
    if (this.currentScene != null) {
      return this.currentScene.render();
    }
  };

  GameManager.prototype.showSceneType = function(sceneType, params) {
    if (sceneType != null) {
      if (this.currentScene != null) {
        this.currentScene.destroy();
      }
      this.currentScene = new sceneType(this.game, params);
      return this.currentScene.start();
    }
  };

  return GameManager;

})();
;var CharacterConfig;

CharacterConfig = (function() {
  function CharacterConfig() {
    this.CHARACTER_TYPES = {
      TACO_MONSTRO: 'TACO_MONSTRO'
    };
  }

  CharacterConfig.prototype.getImageForCharacter = function(characterType) {
    switch (characterType) {
      case this.CHARACTER_TYPES.TACO_MONSTRO:
        return 'taco-monstro';
    }
    return 'test';
  };

  return CharacterConfig;

})();

window.SharedCharacterConfig = new CharacterConfig();
;var SpriteButton,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

SpriteButton = (function(_super) {
  __extends(SpriteButton, _super);

  function SpriteButton(game, x, y, normalImage, pressedImage, title, clickFn) {
    var emptyFn, style, titleText;
    Phaser.Group.call(this, game);
    this.game = game;
    this.normalImage = normalImage;
    this.pressedImage = pressedImage;
    emptyFn = function() {};
    this.clickFn = (clickFn != null) === true ? clickFn : emptyFn;
    this.buttonBackgroundSprite = new Phaser.Sprite(this.game, x, y, this.normalImage);
    this.buttonBackgroundSprite.anchor.setTo(0.5, 0.5);
    style = {
      font: "24px Arial",
      fill: "#ffffff",
      align: "center"
    };
    titleText = title != null ? title : '';
    this.buttonTitleText = new Phaser.Text(this.game, x, y, titleText, style);
    this.buttonTitleText.anchor.set(0.5);
    this.add(this.buttonBackgroundSprite);
    this.add(this.buttonTitleText);
    this.buttonBackgroundSprite.inputEnabled = true;
    this.buttonBackgroundSprite.events.onInputDown.add(function() {
      if (this.pressedImage) {
        return this.buttonBackgroundSprite.loadTexture(this.pressedImage);
      }
    }, this);
    this.buttonBackgroundSprite.events.onInputUp.add(function() {
      this.buttonBackgroundSprite.loadTexture(this.normalImage);
      if (this.clickFn) {
        return this.clickFn();
      }
    }, this);
  }

  return SpriteButton;

})(Phaser.Group);
;var SpriteToggleButton,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

SpriteToggleButton = (function(_super) {
  __extends(SpriteToggleButton, _super);

  function SpriteToggleButton(game, x, y, offImage, onImage, title, clickFn) {
    var emptyFn, style, titleText;
    Phaser.Group.call(this, game);
    this.game = game;
    this.isOn = false;
    this.offImage = offImage;
    this.onImage = onImage;
    emptyFn = function() {};
    this.clickFn = (clickFn != null) === true ? clickFn : emptyFn;
    this.buttonBackgroundSprite = new Phaser.Sprite(this.game, x, y, this.offImage);
    this.buttonBackgroundSprite.anchor.setTo(0.5, 0.5);
    style = {
      font: "24px Arial",
      fill: "#ffffff",
      align: "center"
    };
    titleText = title != null ? title : '';
    this.buttonTitleText = new Phaser.Text(this.game, x, y, titleText, style);
    this.buttonTitleText.anchor.set(0.5);
    this.add(this.buttonBackgroundSprite);
    this.add(this.buttonTitleText);
    this.buttonBackgroundSprite.inputEnabled = true;
    this.buttonBackgroundSprite.events.onInputUp.add(function() {
      this.toggle();
      if (this.clickFn != null) {
        return this.clickFn();
      }
    }, this);
  }

  SpriteToggleButton.prototype.toggle = function() {
    if (this.isOn === true) {
      return this.toggleOff();
    } else {
      return this.toggleOn();
    }
  };

  SpriteToggleButton.prototype.toggleOn = function() {
    this.isOn = true;
    return this.buttonBackgroundSprite.loadTexture(this.onImage);
  };

  SpriteToggleButton.prototype.toggleOff = function() {
    this.isOn = false;
    return this.buttonBackgroundSprite.loadTexture(this.offImage);
  };

  return SpriteToggleButton;

})(Phaser.Group);
;var NetworkManager,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

NetworkManager = (function() {
  function NetworkManager() {
    this.leaveGame = __bind(this.leaveGame, this);
    this.joinGame = __bind(this.joinGame, this);
    this.requestGamesList = __bind(this.requestGamesList, this);
    this.sendMessageToGame = __bind(this.sendMessageToGame, this);
    this.hostNewGame = __bind(this.hostNewGame, this);
    this.onGameData = __bind(this.onGameData, this);
    this.onPlayerJoinedGame = __bind(this.onPlayerJoinedGame, this);
    this.onJoinedGame = __bind(this.onJoinedGame, this);
    this.onGamesListUpdate = __bind(this.onGamesListUpdate, this);
    this.onDisconnect = __bind(this.onDisconnect, this);
    this.onEvent = __bind(this.onEvent, this);
    this.onConnect = __bind(this.onConnect, this);
    this.isConnected = false;
    this.defaultServerAddress = 'http://192.168.0.207:8081';
    this.socket = null;
    this.playerId = null;
    this.gameId = null;
  }

  NetworkManager.prototype.connectToGameServer = function() {
    this.socket = io(this.defaultServerAddress);
    return this.socket.on('connect', this.onConnect);
  };

  NetworkManager.prototype.onConnect = function() {
    this.isConnected = true;
    console.log('Connected to game server');
    this.socket.on('event', this.onEvent);
    return this.socket.on('disconnect', this.onDisconnect);
  };

  NetworkManager.prototype.onEvent = function(data) {
    return console.log('Recieved event');
  };

  NetworkManager.prototype.onDisconnect = function() {
    console.log('Disconnected from game server');
    return this.isConnected = false;
  };

  NetworkManager.prototype.onGamesListUpdate = function(gameList) {
    console.log('Available games:');
    return console.log(gameList);
  };

  NetworkManager.prototype.onJoinedGame = function(data) {
    return console.log('Joined game with playerId ' + data);
  };

  NetworkManager.prototype.onPlayerJoinedGame = function(data) {
    return console.log('Player ' + data + ' joined game');
  };

  NetworkManager.prototype.onGameData = function(data) {};

  NetworkManager.prototype.hostNewGame = function() {
    return this.socket.emit('hostCreateGame', {});
  };

  NetworkManager.prototype.sendMessageToGame = function(message) {
    return this.socket.emit('sendMessageToGame', message);
  };

  NetworkManager.prototype.requestGamesList = function() {
    return this.socket.emit('requestGamesList', {});
  };

  NetworkManager.prototype.joinGame = function(gameId) {
    this.gameId = gameId;
    return this.socket.emit('joinGame', {
      gameId: gameId
    });
  };

  NetworkManager.prototype.leaveGame = function() {
    return this.socket.emit('leaveGame', {});
  };

  return NetworkManager;

})();
;var GameMap,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

GameMap = (function(_super) {
  __extends(GameMap, _super);

  function GameMap(game, mapName) {
    Phaser.Tilemap.call(this, game, mapName, 32, 32, 1024, 768);
  }

  return GameMap;

})(Phaser.Tilemap);
;var PracticeArena,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

PracticeArena = (function(_super) {
  __extends(PracticeArena, _super);

  function PracticeArena(game) {
    PracticeArena.__super__.constructor.call(this, game, 'battleArea1');
    this.addTilesetImage('training-arena-tileset', 'training-arena');
    this.layer1 = this.createLayer('Tile Layer 1');
    this.layer1.resizeWorld();
  }

  return PracticeArena;

})(GameMap);
;var TestMap,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TestMap = (function(_super) {
  __extends(TestMap, _super);

  function TestMap(game) {
    TestMap.__super__.constructor.call(this, game, 'test-map');
    this.addTilesetImage('basic', 'basic-tilemap');
    this.layer1 = this.createLayer('Tile Layer 1');
    this.layer2 = this.createLayer('Tile Layer 2');
    this.layer1.resizeWorld();
    this.layer2.resizeWorld();
  }

  return TestMap;

})(GameMap);
;var GameListScene,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

GameListScene = (function() {
  function GameListScene(game) {
    this.updateGameList = __bind(this.updateGameList, this);
    this.joinedGame = __bind(this.joinedGame, this);
    this.pressedJoinGameButton = __bind(this.pressedJoinGameButton, this);
    this.game = game;
    this.availableGames = ['test', 'test', 'test', 'test', 'test'];
    this.availableGameToggleButtons = [];
    this.currentSelectedGame = null;
  }

  GameListScene.prototype.start = function() {
    this.joinGameButton = new SpriteButton(this.game, this.game.world.centerX - 256, this.game.world.height - 160, 'button-blue', 'button-blue-pressed', 'Join Game', this.pressedJoinGameButton);
    this.backButton = new SpriteButton(this.game, this.game.world.centerX + 256, this.game.world.height - 160, 'button-pink', 'button-pink-pressed', 'Back to Menu', this.pressedBackButton);
    if (SharedGameManager.networkManager.isConnected === true) {
      SharedGameManager.networkManager.socket.on('gamesListUpdate', this.updateGameList);
      return SharedGameManager.networkManager.requestGamesList();
    }
  };

  GameListScene.prototype.destroy = function() {
    var gameToggleButton, _i, _len, _ref, _results;
    if (SharedGameManager.networkManager.socket != null) {
      SharedGameManager.networkManager.socket.removeListener('gamesListUpdate', this.updateGameList);
    }
    this.joinGameButton.destroy(true);
    this.backButton.destroy(true);
    _ref = this.availableGameToggleButtons;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      gameToggleButton = _ref[_i];
      _results.push((function(_this) {
        return function(gameToggleButton) {
          return gameToggleButton.destroy(true);
        };
      })(this)(gameToggleButton));
    }
    return _results;
  };

  GameListScene.prototype.update = function() {};

  GameListScene.prototype.render = function() {};

  GameListScene.prototype.pressedJoinGameButton = function() {
    if ((this.currentSelectedGame != null) === true && SharedGameManager.networkManager.isConnected === true) {
      SharedGameManager.networkManager.socket.on('joinedGame', this.joinedGame);
      return SharedGameManager.networkManager.joinGame(this.currentSelectedGame);
    }
  };

  GameListScene.prototype.pressedBackButton = function() {
    return SharedGameManager.showSceneType(TitleScene);
  };

  GameListScene.prototype.joinedGame = function(data) {
    SharedGameManager.networkManager.socket.removeListener('joinedGame', this.joinedGame);
    return SharedGameManager.showSceneType(MainGameScene, {
      isHost: false,
      playerId: data
    });
  };

  GameListScene.prototype.selectGame = function(selectedButton, selectedGame) {
    var gameToggleButton, _i, _len, _ref, _results;
    this.currentSelectedGame = selectedGame;
    _ref = this.availableGameToggleButtons;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      gameToggleButton = _ref[_i];
      _results.push((function(_this) {
        return function(gameToggleButton) {
          if (gameToggleButton !== selectedButton) {
            return gameToggleButton.toggleOff();
          }
        };
      })(this)(gameToggleButton));
    }
    return _results;
  };

  GameListScene.prototype.updateGameList = function(data) {
    var current, g, gameToggleButton, xCount, yCount, _fn, _i, _j, _len, _len1, _ref, _ref1, _results;
    this.availableGames = JSON.parse(data);
    current = this.currentSelectedGame;
    this.currentSelectedGame = null;
    _ref = this.availableGameToggleButtons;
    _fn = (function(_this) {
      return function(gameToggleButton) {
        return gameToggleButton.destroy(true);
      };
    })(this);
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      gameToggleButton = _ref[_i];
      _fn(gameToggleButton);
    }
    this.availableGameToggleButtons = [];
    yCount = 0;
    xCount = 0;
    _ref1 = this.availableGames;
    _results = [];
    for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
      g = _ref1[_j];
      _results.push((function(_this) {
        return function(g) {
          var toggleButton;
          toggleButton = new SpriteToggleButton(_this.game, 224 + (288 * xCount), 128 + (64 * yCount), 'game-selector-button-off', 'game-selector-button-on', '' + g, null);
          toggleButton.clickFn = function() {
            return _this.selectGame(toggleButton, g);
          };
          _this.availableGameToggleButtons.push(toggleButton);
          if (g === current) {
            _this.selectGame(toggleButton, g);
            toggleButton.toggleOn();
          }
          xCount++;
          if (xCount > 2) {
            xCount = 0;
            return yCount++;
          }
        };
      })(this)(g));
    }
    return _results;
  };

  return GameListScene;

})();
;var MainGameScene,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

MainGameScene = (function() {
  function MainGameScene(game, params) {
    this.onGameData = __bind(this.onGameData, this);
    this.game = game;
    this.isHost = params.isHost;
    this.playerId = this.isHost === true ? 0 : params.playerId;
    this.playersData = new Array(8);
    this.otherPlayers = new Array(8);
    this.networkUpdateTimer = 0;
  }

  MainGameScene.prototype.start = function() {
    this.game.physics.startSystem(Phaser.Physics.ARCADE);
    if (SharedGameManager.networkManager.isConnected === true) {
      SharedGameManager.networkManager.socket.on('gameData', this.onGameData);
    }
    this.setupMap();
    return this.controlledPlayer = new PlayerCharacter(this.game, this.game.world.centerX, this.game.world.centerY, SharedCharacterConfig.CHARACTER_TYPES.TACO_MONSTRO, true);
  };

  MainGameScene.prototype.destroy = function() {};

  MainGameScene.prototype.onGameData = function(data) {
    console.log(data);
    if ((data.playerId != null) === true && data.playerId !== this.playerId) {
      this.playersData[data.playerId] = data.playerData;
      if ((this.otherPlayers[data.playerId] != null) === false) {
        this.otherPlayers[data.playerId] = new PlayerCharacter(this.game, this.game.world.centerX, this.game.world.centerY, data.playerData.characterType, false);
      }
      this.otherPlayers[data.playerId].characterSprite.x = data.playerData.x;
      return this.otherPlayers[data.playerId].characterSprite.y = data.playerData.y;
    }
  };

  MainGameScene.prototype.update = function() {
    if (SharedGameManager.networkManager.isConnected === true && (this.playerId != null) === true) {
      return SharedGameManager.networkManager.sendMessageToGame({
        playerId: this.playerId,
        playerData: {
          characterType: this.controlledPlayer.characterType,
          x: this.controlledPlayer.characterSprite.x,
          y: this.controlledPlayer.characterSprite.y
        }
      });
    }
  };

  MainGameScene.prototype.render = function() {};

  MainGameScene.prototype.setupMap = function() {
    return this.gameMap = new PracticeArena(this.game);
  };

  return MainGameScene;

})();
;var TitleScene;

TitleScene = (function() {
  function TitleScene(game) {
    this.game = game;
  }

  TitleScene.prototype.start = function() {
    this.hostGameButton = new SpriteButton(this.game, this.game.world.centerX, 200, 'button-blue', 'button-blue-pressed', 'Host Game', this.pressedHostGameButton);
    return this.joinGameButton = new SpriteButton(this.game, this.game.world.centerX, 300, 'button-pink', 'button-pink-pressed', 'Join Game', this.pressedJoinGameButton);
  };

  TitleScene.prototype.destroy = function() {
    this.hostGameButton.destroy(true);
    return this.joinGameButton.destroy(true);
  };

  TitleScene.prototype.update = function() {};

  TitleScene.prototype.render = function() {};

  TitleScene.prototype.pressedHostGameButton = function() {
    if (SharedGameManager.networkManager.isConnected === true) {
      SharedGameManager.networkManager.hostNewGame();
    }
    return SharedGameManager.showSceneType(MainGameScene, {
      isHost: true
    });
  };

  TitleScene.prototype.pressedJoinGameButton = function() {
    return SharedGameManager.showSceneType(GameListScene);
  };

  return TitleScene;

})();
;var ExampleScene,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

ExampleScene = (function(_super) {
  __extends(ExampleScene, _super);

  function ExampleScene(game) {
    ExampleScene.__super__.constructor.call(this, game);
    this.logoSprite = null;
  }

  ExampleScene.prototype.start = function() {
    ExampleScene.__super__.start.call(this);
    this.logoSprite = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
    return this.logoSprite.anchor.setTo(0.5, 0.5);
  };

  ExampleScene.prototype.destroy = function() {
    ExampleScene.__super__.destroy.call(this);
    return this.logoSprite.destroy(true);
  };

  ExampleScene.prototype.update = function() {
    return ExampleScene.__super__.update.call(this);
  };

  ExampleScene.prototype.render = function() {
    return ExampleScene.__super__.render.call(this);
  };

  return ExampleScene;

})(Scene);
;window.SharedGameManager = new GameManager(1024, 768, TitleScene);
;
//# sourceMappingURL=Game.js.map