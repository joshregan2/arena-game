class MainGameScene
  constructor: (game, params) ->
    # Initialise scene
    @game = game
    @isHost = params.isHost
    @playerId = if @isHost is true then 0 else params.playerId

    @playersData = new Array(8)
    @otherPlayers = new Array(8)

    @networkUpdateTimer = 0

  start: () ->
    # Perform start up logic here
    @game.physics.startSystem(Phaser.Physics.ARCADE);

    # Networking
    if SharedGameManager.networkManager.isConnected is true
      SharedGameManager.networkManager.socket.on('gameData', @onGameData)

    # Game Setup
    @setupMap()
    @controlledPlayer = new PlayerCharacter(@game, @game.world.centerX, @game.world.centerY, SharedCharacterConfig.CHARACTER_TYPES.TACO_MONSTRO, true)

  destroy: () ->
    # Call this to destroy this and any references

  onGameData: (data) =>
    console.log(data)
    if data.playerId? is true and data.playerId isnt @playerId
      @playersData[data.playerId] = data.playerData
      if @otherPlayers[data.playerId]? is false
        @otherPlayers[data.playerId] = new PlayerCharacter(@game, @game.world.centerX, @game.world.centerY, data.playerData.characterType, false)

      @otherPlayers[data.playerId].characterSprite.x = data.playerData.x
      @otherPlayers[data.playerId].characterSprite.y = data.playerData.y

  update: () ->
    # Update loop

    # Networking
    if SharedGameManager.networkManager.isConnected is true and @playerId? is true
      SharedGameManager.networkManager.sendMessageToGame({
        playerId: @playerId,
        playerData: {
          characterType: @controlledPlayer.characterType,
          x: @controlledPlayer.characterSprite.x,
          y: @controlledPlayer.characterSprite.y
        }
      })

  render: () ->
    # Render loop

  setupMap: () ->
    @gameMap = new PracticeArena(@game)
